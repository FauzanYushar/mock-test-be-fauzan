var express = require("express");
var router = express.Router();
var toDoListController = require("../controllers/toDoListController");

router.post("/:id", toDoListController.add);
router.get("/:id", toDoListController.show);

module.exports = router;
