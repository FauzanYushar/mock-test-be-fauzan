var express = require("express");
var router = express.Router();
var authRouter = require("./authRouter");
var toDoListRouter = require("./toDoListRouter");

router.use("/auth", authRouter);
router.use("/todolist", toDoListRouter);

module.exports = router;
