const { user } = require("../models");

exports.login = async (req, res) => {
  const { pin } = req.body;
  try {
    const userLogin = await user.findOne({ where: { pin } });
    if (!userLogin) {
      res.status(400).json({ message: "Wrong PIN" });
    } else res.status(200).json({ message: "login success", data: userLogin });
  } catch (err) {
    res.status(400).json({ message: "bad gateway", errMsg: err.message });
  }
};

exports.register = async (req, res) => {
  const { username, pin } = req.body;
  const matchUsernameData = await user.findOne({ where: { username } });
  const matchPinData = await user.findOne({ where: { pin } });
  if (matchUsernameData || matchPinData) {
    res
      .status(400)
      .json({ message: "use another combination of username and pin" });
  } else {
    try {
      if (username && pin) {
        const userRegister = await user.create({
          username,
          pin,
        });
        res
          .status(200)
          .json({ message: "post data success", data: userRegister });
      } else res.status(400).json({ message: "fill data first" });
    } catch (err) {
      res.status(400).json({ message: "bad gateway", errMsg: err.message });
    }
  }
};
