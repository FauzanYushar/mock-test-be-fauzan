const { toDoList } = require("../models");

exports.add = async (req, res) => {
  const { id } = req.params;
  const { todo } = req.body;
  console.log(todo);
  try {
    const addToDoList = await toDoList.create({
      user_id: id,
      todo,
      check: false,
    });
    res.status(200).json({ message: "add data success", data: addToDoList });
  } catch (err) {
    res.status(400).json({ message: "bad gateway", errMsg: err.message });
  }
};

exports.show = async (req, res) => {
  const { id } = req.params;
  try {
    const showToDoList = await toDoList.findAll({ where: { user_id: id } });
    res.status(200).json({ message: "get data success", data: showToDoList });
  } catch (err) {
    res.status(400).json({ message: "bad gateway", errMsg: err.message });
  }
};
