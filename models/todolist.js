'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class toDoList extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  toDoList.init({
    user_id: DataTypes.INTEGER,
    todo: DataTypes.STRING,
    check: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'toDoList',
  });
  return toDoList;
};